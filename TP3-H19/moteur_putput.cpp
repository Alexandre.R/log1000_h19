#include "moteur_putput.h"

Putput::Putput(int numero_serie) : Moteur(numero_serie) {
    // Les moteurs Putput ont deux pistons de 30cm3.
    this->pistons.push_back(new Piston(30));
    this->pistons.push_back(new Piston(30));  
}

int Putput::getPuissance() {
    // Les moteurs Putput ont une puissance de 2cv.
    return 2;
}

